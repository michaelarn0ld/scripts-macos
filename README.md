# MacOS Bash Scripts
----
This project serves as a means to store and share the Bash scripts and command
line tools that I use on my MacOS machine. If you are interested in the Bash scripts
that I use on my Linux machine, you can find them [here](https://gitlab.com/michaelarn0ld/bash-scripts-linux).

If you intend to use any of these scripts, it is important to ensure that they
are accessible by your ```$PATH```.

In the first line of every file you will find the interpreter path.  If you are
not using bash via homebrew, please replace the path to the interpreter:

***FROM***
```bash
#!/opt/homebrew/bin/bash
```
***TO***
```bash
#!/bin/bash
```


# Table of Contents
----
1. [ddg](#ddg)
1. [reactcomp](#reactcomp)

|   Scripts   |   Summary                                                      |
|   :-:       |   -                                                            |
|   ddg       |   Performs a DuckDuckGo search from the terminal               |
|   reactcomp |   Boiler-plate for react functional components


## ddg
----
ddg is a command line tool that allows you to make DuckDuckGo searches right
from the terminal! Simply excecute ```ddg``` followed by what you want to
search for, and let the magic happen.

### KNOWN ISSUES
If you do not already have an instance of the browser running, ```ddg``` (and
```open``` for that matter) will open two browser windows.

### GETTING STARTED
* Determine the location of bash by running: ```which bash```

### CONFIGURATION
* Change ```#!/opt/homebrew/bin/bash``` to the location of your ```bash```

### USAGE
```bash
ddg [ARGS...]
```


## reactcomp
---
Reactcomp is a tool that takes a single argument and generate the basic
boiler-plate for a react component.

### USAGE
```bash
reactcomp [NAME FOR COMPONENT]
```


# Contributors
----
@author: Michael Arnold \
@contact: me@michaelarnold.io


# License
----
Copyright © 2021 Michael Arnold

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
